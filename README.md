# NLP - Translation using seq2seq and transformers
This project contains an implementation of a model for neural machine translation.
Writing parts of this project was one of the tasks during NLP 2022 course at MIMUW.
The whole description of the task can be found in the file <code>practical_3.pdf</code>
Analysis of results can be found in the file <code>report.pdf</code>
